abstract class GirlFriend {      //抽象类，封装了两个行为标准
    abstract void speak();
    abstract void cooking();
}
class ChinaGirlFriend extends GirlFriend {
    void speak() {
        System.out.println("您好");
    }
    void cooking() {
        System.out.println("水煮鱼");
    }
}
class AmericanGirlFriend extends GirlFriend {
    void speak() {
        System.out.println("hello");
    }
    void cooking() {
        System.out.println("roast beef");
    }
}
class Boy {
    GirlFriend friend;
    void setGirlFriend(GirlFriend f) {
        friend = f;
    }
    void showGirlFriend() {
        friend.speak();
        friend.cooking();
    }
}
public class Example5_12 {
    public static void main(String args[]) {
        GirlFriend gril = new ChinaGirlFriend();   //gril是上转型对象
        Boy boy = new Boy();
        boy.setGirlFriend(gril);
        boy.showGirlFriend();
        gril = new AmericanGirlFriend();            //gril是上转型对象
        boy.setGirlFriend(gril);
        boy.showGirlFriend();
    }
}

