public class Calc {
    public static void main(String[] args) {
        int result = 0;
        if (args.length != 3) {
            System.out.println("Usage: java Calc operato1 operand(+ - * / %) operator2");
        }
        else {
            if (args[1].equals("+")) {
                result = Integer.parseInt(args[0]) + Integer.parseInt(args[2]);
                System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
            } else if (args[1].equals("-")) {
                result = Integer.parseInt(args[0]) - Integer.parseInt(args[2]);
                System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
            } else if (args[1].equals("x")) {
                result = Integer.parseInt(args[0]) * Integer.parseInt(args[2]);
                System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
            } else if (args[1].equals("/")) {
                result = Integer.parseInt(args[0]) / Integer.parseInt(args[2]);
                System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
            } else {
                result = Integer.parseInt(args[0]) % Integer.parseInt(args[2]);
                System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
            }
        }
    }
}